package com.cheri.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: TODO
 * @ClassName UserCrudApplication
 * @Author Cheri
 * @Date 2019/7/29 - 17:17
 * @Version V1.0
 **/
@SpringBootApplication
public class UserCrudApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserCrudApplication.class,args);
    }
}
