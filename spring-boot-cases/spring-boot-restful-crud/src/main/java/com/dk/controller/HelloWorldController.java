package com.dk.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/4 - 16:53
 * @Version V1.0
 **/
@RequestMapping("/demo")
@RestController
public class HelloWorldController {

    @RequestMapping("/hello")
    public String hello(){
        return "hello-world-springboot";
    }
}
