package com.dk.dao;

import com.dk.bean.User;
import org.apache.ibatis.annotations.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/4 - 17:14
 * @Version V1.0
 **/
@Mapper
public interface UserDao {
    /**
     * 用户数据新增
     */
    @Insert("insert into t_user(id,name,age) values (#{id},#{name},#{age})")
    void addUser(User user);
    /**
     * 用户数据修改
     */
    @Update("update t_user set name=#{name},age=#{age} where id=#{id}")
    void updateUser(User user);
    /**
     * 用户数据删除
     */
    @Delete("delete from t_user where id=#{id}")
    void deleteUser(int id);
    /**
     * 根据用户名称查询用户信息
     *
     */
    @Select("SELECT id,name,age FROM t_user")
    // 返回 Map 结果集
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age"),
    })
    User findByName(@Param("name") String userName);
    /**
     * 根据用户ID查询用户信息
     *
     */
    @Select("SELECT id,name,age FROM t_user")
    User findById(@Param("id") int userId);
    /**
     * 根据用户age查询用户信息列表
     */
    @Select("SELECT id,name,age FROM t_user where age = #{userAge}")
    List<User> findByAge(int userAge);
}