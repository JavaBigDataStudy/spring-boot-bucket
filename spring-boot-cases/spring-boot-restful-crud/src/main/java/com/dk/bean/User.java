package com.dk.bean;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/4 - 17:11
 * @Version V1.0
 **/
public class User {

    /** 编号 */
    private int id;
    /** 姓名 */
    private String name;

    /** 年龄 */
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
