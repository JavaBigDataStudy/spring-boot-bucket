package com.dk.service;

import com.dk.bean.User;

import java.util.List;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/4 - 17:21
 * @Version V1.0
 **/
public interface UserService {
    /**
     * 新增用户
     * @param user
     * @return
     */
    boolean addUser(User user);
    /**
     * 修改用户
     * @param user
     * @return
     */
    boolean updateUser(User user);
    /**
     * 删除用户
     * @param id
     * @return
     */
    boolean deleteUser(int id);
    /**
     * 根据用户名字查询用户信息
     * @param userName
     */
    User findUserByName(String userName);
    /**
     * 根据用户ID查询用户信息
     * @param userId
     */
    User findUserById(int userId);
    /**
     * 根据用户年龄查询用户信息
     * @param userAge
     */
    List<User> findUserByAge(int userAge);
}
