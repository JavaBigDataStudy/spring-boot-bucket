package com.dk.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/3 - 19:30
 * @Version V1.0
 **/
@SpringBootApplication
public class SpringBootSeckillApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSeckillApplication.class,args);
    }

}
