##spring-boot-seckill 项目简要说明

 - 业务需求简介：
 
   1.高并发场景
   
   2.同一用户只能抢购到同一商品一次
   
   3.保证业务能及时反馈给用户处理情况
 - 1.Dao层设计
 - 2 Service层设计
 - 3 Controller层设计
- 具体业务需求与设计文档 参考如下文档：

   ### 项目文档地址:F:\MyCoderLife\Doc
   1.需求规格说明书.doc
   
   2.系统概要设计.doc
   
   3.系统详细设计.doc