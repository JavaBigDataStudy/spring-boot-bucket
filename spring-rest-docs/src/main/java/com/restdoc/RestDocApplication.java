package com.restdoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring rest doc例子
 */
@SpringBootApplication
public class RestDocApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestDocApplication.class, args);
	}
}
