package com.restdoc;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/2 - 4:31
 * @Version V1.0
 **/
public class StaticMethod {

    public static void main(String[] args) {
        StaticMethod staticMethod = new StaticMethod();
        staticMethod.delete();
        StaticMethod.delete2();
    }

    private void delete (){
        System.out.println(111);
    }

    private static void delete2 (){
        System.out.println(111);
    }
}
