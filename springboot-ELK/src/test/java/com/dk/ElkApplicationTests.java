package com.dk;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/2 - 10:05
 * @Version V1.0
 **/
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElkApplicationTests {

    // 定义一个全局的记录器，通过LoggerFactory获取
    private final static Logger log = LoggerFactory.getLogger(Test.class);

    @Before
    public void setUp() {
    }
    @Test
    public void test() {
        log.trace("trace 成功了");
        log.debug("debug 成功了");
        log.info("info 成功了");
        log.warn("warn 成功了");
        log.error("error 成功了");
        log.info("ELK-日志测试记录！。。");
    }

    @Test
    public void test2() {
        ElkApplicationTests obj = new ElkApplicationTests();
        try{
            obj.divide();
        }catch(ArithmeticException ex){
            log.error("大家好111!", ex);
        }
    }
    private void divide(){
        int i = 10 /0;
    }

}
