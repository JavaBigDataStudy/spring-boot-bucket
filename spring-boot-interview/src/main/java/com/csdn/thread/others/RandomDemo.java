package com.csdn.thread.others;

import java.util.Random;

/**
 * @Description: 演示Random生成伪随机数用法
 * @ClassName RandomDemo
 * @Author Cheri
 * @Date 2019/7/30 - 17:45
 * @Version V1.0
 **/
public class RandomDemo {
    public static void main(String[] args) {

        //如下代码产生相同的随机数
//        for (int i = 0; i < 100; i++) {
//            long currentTimeMillis = System.currentTimeMillis();
//            System.out.println(currentTimeMillis);
//            Random r = new Random(currentTimeMillis);
//            System.out.println(r.nextInt());
//        }

        //如下代码产生不同的随机数
        for (int i = 0; i < 20; i++) {
            Random r = new Random();
            System.out.println(r.nextInt());
            int nextInt = new Random(System.currentTimeMillis() % 10).nextInt(10);
            System.out.println(nextInt);
        }
//
//        System.out.println("=========3-[3]===================>");
//        Random random = new Random(10);
//        for (int i = 0; i < 3; i++) {
//            System.out.println(random.nextInt());
//        }
    }
}
