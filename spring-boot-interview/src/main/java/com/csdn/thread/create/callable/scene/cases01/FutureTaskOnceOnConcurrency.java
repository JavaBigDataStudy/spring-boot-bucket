package com.csdn.thread.create.callable.scene.cases01;

import com.csdn.thread.ConnectUtil;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description: TODO
 * @ClassName FutureTaskOnceOnConcurrency
 * @Author Cheri
 * @Date 2019/7/31 - 11:18
 * @Version V1.0
 **/
public class FutureTaskOnceOnConcurrency {

    private Map<String, Connection> connectionPool = new HashMap<String, Connection>();
    private ReentrantLock lock = new ReentrantLock();

    public Connection getConnection(String key) {
        try {
            lock.lock();
            if (connectionPool.containsKey(key)) {
                return connectionPool.get(key);
            } else {
                //创建 Connection
                Connection conn = createConnection();
                connectionPool.put(key, conn);
                return conn;
            }
        } finally {
            lock.unlock();
        }
    }

    //创建Connection
    private Connection createConnection() {
        return ConnectUtil.Connect();
    }

    public static void main(String[] args) {
        FutureTaskOnceOnConcurrency onceOnConcurrency = new FutureTaskOnceOnConcurrency();
        Connection connection = onceOnConcurrency.getConnection("test");
        System.out.println(connection);
    }
}
