package com.csdn.thread.create.callable.scene;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @Description: 线程查询
 * @ClassName ThreadQueryCallable
 * @Author Cheri
 * @Date 2019/7/31 - 1:35
 * @Version V1.0
 **/
public class ThreadQueryCallable implements Callable<List> {

    private String search;//查询条件 根据条件来定义该类的属性
    private int bindex;//当前页数
    private int num;//每页查询多少条
    private String table;//要查询的表名，也可以写死，也可以从前面传
    private List page;//每次分页查出来的数据

    public  ThreadQueryCallable(int bindex,int num,String table) {
        this.bindex=bindex;
        this.num=num;
        this.table=table;
        //分页查询数据库数据
//        page=sqlHadle.queryTest11(bindex,num,table);
    }

    @Override
    public List call() throws Exception {
        return page;
    }
}
