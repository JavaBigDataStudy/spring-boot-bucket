package com.csdn.thread.create.callable.demo;

import java.util.concurrent.*;

/**
 * @Description: TODO
 * @ClassName CallableAndFutureTask
 * @Author Cheri
 * @Date 2019/7/30 - 2:14
 * @Version V1.0
 **/
public class CallableAndFutureTask {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Task task = new Task();
        FutureTask<Integer> futureTask = new FutureTask<Integer>(task);
        executorService.submit(futureTask);
        executorService.shutdown();

        System.out.println("主线程在执行任务...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        try {
            System.out.println("task运行结果:" + futureTask.get());
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        }

        System.out.println("所有任务执行完毕");
    }
}

