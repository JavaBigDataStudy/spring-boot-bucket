package com.csdn.thread.create.callable.scene.cases01;

import com.csdn.thread.ConnectUtil;

import java.sql.Connection;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.FutureTask;

/**
 * @Description: TODO
 * @ClassName NoLockSafeFuture
 * @Author Cheri
 * @Date 2019/7/31 - 11:28
 * @Version V1.0
 **/
public class NoLockSafeFuture {

    private ConcurrentHashMap<String, FutureTask<Connection>> connectionPool = new ConcurrentHashMap<String, FutureTask<Connection>>();

    public Connection getConnection(String key) throws Exception {
        FutureTask<Connection> connectionTask = connectionPool.get(key);
        if (connectionTask != null) {
            return connectionTask.get();
        } else {
            Callable<Connection> callable = new Callable<Connection>() {
                @Override
                public Connection call() throws Exception {
                    return createConnection();
                }
            };
            FutureTask<Connection> newTask = new FutureTask<Connection>(callable);
            connectionTask = connectionPool.putIfAbsent(key, newTask);
            if (connectionTask == null) {//进行添加
                connectionTask = newTask;
                connectionTask.run();//创建连接
            }
            return connectionTask.get();
        }
    }

    //创建Connection
    private Connection createConnection() {
        return ConnectUtil.Connect();
    }

    public static void main(String[] args) throws Exception {
        NoLockSafeFuture noLockSafeFuture = new NoLockSafeFuture();
        Connection connection = noLockSafeFuture.getConnection("test");
        System.out.println(connection);
        System.out.println(noLockSafeFuture.connectionPool);
    }
}
